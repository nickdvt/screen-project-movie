$('.list-item-phim-dang-chieu').owlCarousel({
    loop: true,
    margin:10,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
  })